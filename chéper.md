Liste d’artistes chéper
=======================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Chéper

Tous les artistes qui donnent l’impression d’avoir pris des champis, avec des shakra du troisième œil et des fractales, etc.

- Alex Grey
- Amanda Sage
- Barbara Takenaga