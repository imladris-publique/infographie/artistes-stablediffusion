Liste d’artistes ethniques
==========================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Ethnique

Tous les artistes représentant des personnes et des paysages très marqués d’une culture non européenne (dont amérique d’ailleurs).

#### Afrique

- Amadou Opa Bathily

#### Moyen Orient

- Abdel Hadi Al Gazzar
- Abed Abdi
- Aminollah Rezaei

#### Asie
##### Japon

- Ando Fuchs
- Audrey Kawasaki
- Ayami Kojima
- Bakemono Zukushi

##### Chine

- Ai Weiwei
- Angela Sung
- Bo Chen -> vérifier si pas doublon avec Bayard Wu dans fantasy, ou autre ici

##### Inde

- Ayan Nag
- Bhupen Khakhar

##### Asie du sud-est

- Affandi

#### Amérique

- Alberto Vargas
- Alice Neel
- Allen Williams
- Art Frahm
- Barkley L. Hendricks
- Beauford Delaney
- Bill Traylor
- Brian Stelfreeze

#### Autre, imprécis

- Aggi Erguna
- Alan Bean
- Amedee Ozenfant
