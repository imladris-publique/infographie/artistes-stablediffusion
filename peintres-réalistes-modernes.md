Liste d’artistes peintres réalistes modernes
============================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Peintres réalistes modernes

Tous les peintres et dessinateurs type fusain plus modernes qui représentent des choses réalistes et notamment des paysages naturels.

- Adam Paquette -> doublon avec Ashley Willerton, ressemble un peu à Andrei Markin aussi, et à Alpo Jaakola, très proche aussi de Ben Hatke
- Adrian Donoghue
- Adrian Paul Allinson
- Agnes Lawrence Pelton
- Alan Parry
- Alpo Jaakola
- Amiet Cuno
- Andrei Markin
- Andrey Remnev
- Anita Malfatti
- Anja Percival
- Anka Zhuravleva
- Ann Stookey
- Anselm Kiefer
- Arkhyp Kuindzhi
- Art Fitzpatrick
- Artem Chebokha
- Arthur Lismer
- Arthur Streeton
- Ashley Willerton
- Atey Ghailan
- Austin Osman Spare
- Basil Gogos
- Ben Hatke -> vérifier que pas trop proche d’Anja Percival, mais je crois que ça va, par contre doublon avec Ashley Willerton/Adam Paquette, vérifier
- Ben Quilty
- Ben Wooten
- Benoit B. Mandelbrot
- Bill Brauer
- Bill Medcalf -> vérifier si doublon
- Billy Childish
- Bo Bartlett
- Bob Ross
- Brent Cotton -> gros risque de doublons avec les Adam Paquette like
- Bruce Coville -> attention préciser «sans écriture» pour pas avoir le nom de l’artiste

#### Top 12

- Alan Parry
- Alpo Jaakola
- Amiet Cuno
- Andrei Markin
- Anja Percival
- Anka Zhuravleva
- Anselm Kiefer
- Art Fitzpatrick
- Ashley Willerton
- Atey Ghailan
- Billy Childish
- Bob Ross