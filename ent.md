Liste d’artistes représentant des ents
======================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Ent

Tous les artistes représentant des femmes ayant des fleurs voire des ramures d’arbre dans la chevelure, voire des êtres mi-humains mi-végétal. Certains peuvent présenter une silhouette de fille en robe rouge au loin.

- Amandine Van Ray
- Amy Judd
- Alexander McQueen
- Annie Soudain
- Bess Hamiti
- Brad Kunkle
- Brian Mashburn

#### Ent de fantasy

Ici les artistes représentant des ents dans un style de fantasy.

- Aaron Horkey
- Alexander Jansson
- Amanda Clark
- Andy Kehoe

#### Filles en robe rouge

Ici les artistes représentant des ents avec une silhouette de fille en robe rouge au loin.

- Aliza Razell
- Bella Kotak
- Beth Conklin
- Brooke Shaden