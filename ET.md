Liste d’artistes surréalistes
=============================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### E.T.

Tous les artistes qui représentent des visages longilignes, souvent chauves, avec un look un peu extraterrestres ou cancéreux.

- Adam Martinakis
- Afarin Sajedi
- Anton Semenov
- Bruno Walpoth

### Top (en cours)

- Adam Martinakis
- Afarin Sajedi
- Anton Semenov