Liste d’artistes représentant des personnages potelés
=====================================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Potelé

Tous les artistes représentant des personnages potelés, parfois difformes, avec des paysages aux formes très arrondies ou ovoïdes.

- Alan Kenny
- Botero
- Bridget Bate Tichenor