Liste d’artistes steampunk
==========================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Steampunk

Tous les artistes dans un style un peu steampunk.

- Alejandro Burdisio
- Alex Andreev
- Andy Fairhurst
- Angus McKie
- Brian Despain
- Bruce Pennington
- Alfred Kubin

### Top (en cours)

- Alejandro Burdisio
- Alex Andreev
- Brian Despain
- Bruce Pennington