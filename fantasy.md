Liste d’artistes de fantasy
===========================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Fantasy

Tous les dessinateurs de fantasy, ainsi que les dessinateurs de paysages pouvant convenir à de la fantasy (voir également ent, aura).

- Abigail Larson
- Al Williamson
- Andreas Franke
- Anne Mccaffrey
- Aquirax Uno
- Artgerm
- Arthur Hughes
- Arthur Rackham
- Bill Carman
- Brian Kesinger
- Brothers Grimm
- Brothers Hildebrandt

#### Aos sidhes

Dans cette sous-catégorie tous les artistes représentant des êtres de la féérie.

- Adonna Khare
- Agostino Arrivabene
- Anne Stokes
- Anton Pieck
- Brian Froud

#### Héroic fantasy

Ici les artistes avec un style héroic fantasy marqué, avec des chevaliers ou des guerriers en armure par exemple, ou encore des châteaux.

- Adrian Smith
- Andreas Rocha
- Barry Windsor Smith
- Bart Sears
- Bayard Wu

#### Héroic féérie

Ici les artistes qui représentent des aos sidhes en armures et épées, typiquement des combats d’elfes.

- Alan Lee
- Alayna Lemmer

#### Monstres

Ici les orques, les créatures à tentacules et autres styles mortal kombat.

- Aleksi Briclot
- Alex Figini
- Alex Horley
- Antonio J. Manzanedo
- Bob Eggleton

#### Art nouveau

Ici les artistes du style art nouveau.

- Alphonse Mucha
- Antoni Gaudi

#### Réaliste

Ici les artistes ayant un style réaliste, paysages comme personnages, mais avec un côté fantasy.

- Adi Granov
- Alan Moore
- Anatoly Metlan
- Andre Norton
- Andrew Ferez
- Bernie Wrightson
- Boris Vallejo

#### Top 10

- Alan Lee
- Alayna Lemmer
- Anne Stokes
- Antonio J. Manzanedo
- Artgerm
- Barry Windsor Smith
- Bayard Wu
- Bill Carman
- Brian Froud
- Brian Kesinger