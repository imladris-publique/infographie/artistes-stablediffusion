Liste d’artistes peintres réalistes
===================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Peintres réalistes

Tous les peintres un peu anciens, de style aussi bien peintre italien renaissance qu’impressionistes, à condition qu’ils représentent des choses réalistes. Avec des paysages naturels

- Abbott Fuller Graves
- Abbott Handerson Thayer
- Abram Efimovich Arkhipov
- Adam Elsheimer
- Adolf Hirémy-Hirschl -> à retirer, doublon
- Adrianus Eversen
- Albert Bierstadt
- Albert Goodwin
- Albert Lynch
- Albert Pinkham Ryder
- Albrecht Durer
- Alexandr Averin
- Alexei Harlamoff
- Alfred Augustus Glendening
- Alfred Munnings
- Ambrosius Bosschaert
- Andre-Charles Boulle
- Andreas Achenbach
- Anna Ancher
- Anne-Louis Girodet
- Annibale Carracci
- Annie Swynnerton
- Antoine Blanchard
- Antonello da Messina
- Antonio Canova
- Apollinary Vasnetsov
- Armand Guillaumin
- Arthur Hacker
- Barthel Bruyn the Younger
- Berthe Morisot

#### Top 8

- Adam Elsheimer
- Albert Bierstadt
- Albert Goodwin
- Alexandr Averin
- Antoine Blanchard
- Apollinary Vasnetsov
- Armand Guillaumin
- Arthur Hacker
