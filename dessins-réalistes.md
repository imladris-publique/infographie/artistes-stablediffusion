Liste d’artistes dessinateurs réalistes
=======================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Dessins réalistes

Tous les dessinateurs un peu anciens (fusain notamment), de style aussi bien peintre italien renaissance qu’impressionistes, à condition qu’ils représentent des choses réalistes. Avec des paysages naturels

- Adriaen van Outrecht
- Anders Zorn
- Andrea Mantegna
- Andreas Vesalius
- Angela Barrett

### Top (en cours)

- Adriaen van Outrecht
- Andrea Mantegna
- Andreas Vesalius