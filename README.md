# Artistes stablediffusion

Il s’agit d’une préselection de noms d’artistes stablediffusion qui sont suffisamment différents les uns des autres afin de faire des préselections en terme de charte graphique.

# Attention, je n’ai fait que les artistes en A et B pour l’instant

Tous les artistes connus de stablediffusion ne sont pas mis, il s’agit d’une sélection.

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Choix d’un artiste

Vous pouvez choisir un artiste en utilisant le script :

https://framagit.org/imladris-publique/infographie/choix-artistes-sd.git

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif. Chaque catégorie a son fichier.

## Liste des catégories

### Moderne

Tous les artistes dont le style est un peu moderne, urbain, carré… Ou un style un peu dystopique, comme dans les films suédois, ou encore un côté bande dessinées modernes, caricatures… Bref c’est un peu du inclassable mais ça fait moderne.

### Dystopie

Tous les artistes ayant un style un peu dystopie suédoise, type visage de personne modèle au regard vide, avec des couleurs pastel et parfois un fond pop art derrière.

### Fantasy

Tous les dessinateurs de fantasy, ainsi que les dessinateurs de paysages pouvant convenir à de la fantasy.

### Ent

Tous les artistes représentant des femmes ayant des fleurs voire des ramures d’arbre dans la chevelure, voire des êtres mi-humains mi-végétal. Certains peuvent présenter une silhouette de fille en robe rouge au loin.

### Aura

Tous les artistes représentant des personnages (souvent de fantasy) un peu éthérés, avec une sorte d’aura brumeuse derrière.

### SF

Tous les artistes de SF

### Steampunk

Tous les artistes dans un style un peu steampunk.

### Manga

Tous les mangakas.

### Comics

Tous les dessinateurs de comics.

### Peintres réalistes

Tous les peintres un peu anciens, de style aussi bien peintre italien renaissance qu’impressionistes, à condition qu’ils représentent des choses réalistes. Avec des paysages naturels

### Dessins réalistes

Tous les dessinateurs un peu anciens (fusain notamment), de style aussi bien peintre italien renaissance qu’impressionistes, à condition qu’ils représentent des choses réalistes. Avec des paysages naturels

### Peintres réalistes modernes

Tous les peintres et dessinateurs type fusain plus modernes qui représentent des choses réalistes et notamment des paysages naturels.

### Photos

Tous les photographes.

### Ethnique

Tous les artistes représentant des personnes et des paysages très marqués d’une culture non européenne (dont amérique d’ailleurs), classés par continent, voire par pays.

### Chéper

Tous les artistes qui donnent l’impression d’avoir pris des champis, avec des shakra du troisième œil et des fractales, etc.

### Surréaliste

Tous les artistes wtf.

### Potelé

Tous les artistes représentant des personnages potelés, parfois difformes, avec des paysages aux formes très arrondies ou ovoïdes.

### Lignes

Tous les artistes qui représentent des lignes horizontales (parfois verticales) ou des quadrillages, voire des diagonales.

### E.T.

Tous les artistes qui représentent des visages longilignes, souvent chauves, avec un look un peu extraterrestres ou cancéreux.

### Sculpture

Tous les artistes sculpteurs ou représentant les personnages de façon très artificielle.

### Pop art

Tous les artistes de style pop art.

### Cubistes

Tous les artistes ayant un style un peu cubiste.

### Coulée d’encre

Tous les peintres modernes qui laisse couler la gouache pour donner un effet sur les visages, ou les dessinateurs qui laissent des traits de crayon et une colorisation inhomogène pour donner un effet dessiné.

### Illustrations jeunesse

Tous les artistes illustrateurs de livres pour enfants.

### Styles uniques

Tous les artistes inclassables, ayant un style très marqué et très différents les uns des autres.
