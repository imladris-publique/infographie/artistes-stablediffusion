Liste d’artistes de styles uniques
==================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Styles uniques

Tous les artistes inclassables, ayant un style très marqué et très différents les uns des autres.

- Alberto Burri
- Amy Earles
- Aubrey Beardsley
- Barbara Kruger
- Bijou Karman
- Bruno Munari