Liste d’artistes surréalistes
=============================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Sculpture

Tous les artistes sculpteurs ou représentant les personnages de façon très artificielle.

- Alberto Giacometti
- Andy Goldsworthy
- Antony Gormley
- Aron Demetz
- Artur Bordalo
- Barbara Hepworth
- Bordalo II
- Bruce Munro
- Bruce Nauman
- Bruno Catalano

### Top 7

- Andy Goldsworthy
- Antony Gormley
- Aron Demetz
- Artur Bordalo
- Bordalo II
- Bruce Munro
- Bruno Catalano