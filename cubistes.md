Liste d’artistes cubistes
=========================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Cubistes

Tous les artistes ayant un style un peu cubiste.

- Adolph Gottlieb
- Albert Gleizes
- Alberto Magnelli
- Alexander Archipenko
- Alexej von Jawlensky
- Amedeo Modigliani
- Amy Sillman
- André Masson
- Aries Moross
- Arthur Dove
- Asger Jorn
- August Macke
- Auguste Herbin
- Barbara Stauffacher Solomon
- Ben Nicholson
- Ben Shahn
- Bernard Buffet

### Top 9

- Albert Gleizes
- Amy Sillman
- André Masson
- Arthur Dove
- Asger Jorn
- August Macke
- Auguste Herbin
- Barbara Stauffacher Solomon
- Bernard Buffet