Liste d’artistes qui font des lignes
====================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Lignes

Tous les artistes qui représentent des lignes horizontales (parfois verticales) ou des quadrillages, voire des diagonales.

- Agnes Martin
- Alfredo Jaar
- Alma Thomas
- Anne Truitt
- Anni Albers
- Barnett Newman
- Brice Marden
- Bridget Riley

### Top (en cours)

- Anni Albers
- Barnett Newman
- Brice Marden
- Bridget Riley