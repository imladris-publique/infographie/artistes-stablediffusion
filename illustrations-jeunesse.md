Liste d’artistes illustrateurs jeunesse
=======================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Illustrations jeunesse

Tous les artistes illustrateurs de livres pour enfants.

- Allie Brosh
- Beatrix Potter
- Bob Byerley