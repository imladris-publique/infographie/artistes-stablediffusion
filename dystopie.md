Liste d’artistes surréalistes dystopiques
=========================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Dystopie

Tous les artistes ayant un style un peu dystopie suédoise, type visage de personne modèle au regard vide, avec des couleurs pastel et parfois un fond pop art derrière.

- Alex Garant
- Alex Gross
- Anthony Gerace
- Barry McGee