Liste d’artistes en coulée d’encre
==================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Coulée d’encre

Tous les peintres modernes qui laisse couler la gouache pour donner un effet sur les visages, ou les dessinateurs qui laissent des traits de crayon et une colorisation inhomogène pour donner un effet dessiné.

- Adrian Ghenie
- Agnes Cecile
- Alberto Seveso
- Alex Maleev
- Alice Pasquini
- Andre Kohn -> ressemble un peu à Brent Heighton, virer ?
- Andrew Atroshenko
- Anna Bocek
- Anna Razumovskaya
- Anne Bachelier
- Arthur Boyd
- Ashley Wood
- Ben Aronson
- Ben Templesmith
- Benedick Bana
- Bill Sienkiewicz
- Blek Le Rat
- Brent Heighton -> ressemble à Andrew Atroshenko
- Brian M. Viveros -> TODO: créer un style gothique ?

### Top 9

- Agnes Cecile
- Alberto Seveso
- Alice Pasquini
- Anna Bocek
- Anna Razumovskaya
- Anne Bachelier
- Ben Templesmith
- Blek Le Rat
- Brent Heighton