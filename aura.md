Liste d’artistes représentant des personnages avec une aura
============================================================================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Aura

Tous les artistes représentant des personnages (souvent de fantasy) un peu éthérés, avec une sorte d’aura brumeuse derrière.

- Android Jones
- Anna Dittmann
- Bastien Lecouffe-Deharme
- Berndnaut Smilde
- Bojan Jevtic