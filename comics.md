Liste d’artistes de comics
==========================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Comics

Tous les dessinateurs de comics.

- Al Capp
- Al Feldstein
- Alan Davis
- Alex Ross
- Alex Toth
- Art Spiegelman
- Arthur Adams
- Becky Cloonan
- Bill Watterson
- Brian Bolland
- Brian K. Vaughan
- Bruce Timm
- Bryan Hitch
- Butcher Billy

### Top 6

- Alex Toth
- Arthur Adams
- Becky Cloonan
- Brian K. Vaughan
- Bruce Timm
- Bryan Hitch