Liste d’artistes surréalistes
=============================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Surréaliste

Tous les artistes wtf.

- Alastair Magnaldo
- Alvar Aalto
- Antonio Mora -> effet de mouvement, un style ?
- Beeple
- Ben Goossens
- Bert Stern
- Bill Viola
- Brett Whiteley -> je pense qu’il y a un style commun avec des style-unique, les petits gribouillis à côté, à vérifier