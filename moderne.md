Liste d’artistes modernes
=========================

# Ressources

Le lien pour accéder à tous les artistes est ici :
- https://stablediffusion.fr/artists

Attention les artistes changent régulièrement.

# Catégories d’artistes

Les artistes seront classés par catégories, c’est à dire par style général. Dans chaque catégorie il y aura le top 6 (à 9) représentatif.

# Artistes

### Moderne

Tous les artistes dont le style est un peu moderne, urbain, carré… Ou un style un peu dystopique, comme dans les films suédois, ou encore un côté bande dessinées modernes, caricatures… Bref c’est un peu du inclassable mais ça fait moderne.

- Aaron Douglas
- Aaron Jasinski
- Adam Hughes
- Adrian Tomine
- Albert Koetsier
- Albert Marquet
- Alberto Biasi
- Alessandro Gottardo
- Alex Hirsch
- Alex Katz
- Alison Bechdel
- Andrew Macara
- Apollonia Saintclair
- Aron Wiesenfeld
- Asaf Hanuka
- Banksy

#### Top 6

- Aaron Douglas
- Adrian Tomine
- Alison Bechdel
- Apollonia Saintclair
- Asaf Hanuka
- Banksy